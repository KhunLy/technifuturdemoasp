﻿using GestionStock.ASP.Models;
using GestionStock.BLL.BusinessObjects;
using GestionStock.BLL.Exceptions;
using GestionStock.BLL.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestionStock.ASP.Controllers
{
    public class ClientController : Controller
    {
        // GET: Client
        public ActionResult Index()
        {
            // "se connecter" au clientService 
            ClientService service = new ClientService();
            // récupérer tous les clients actifs et mapper les proprités dans un autre objet
            IEnumerable<ClientIndexModel> model = service.GetActiveClients()
                .Select(c => new ClientIndexModel
                {
                    Reference = c.Reference,
                    Nom = c.Nom,
                    Prenom = c.Prenom,
                });
            return View(model);
        }

        // affiche du forulaire
        public ActionResult Create()
        {
            return View();
        }

        //traitement du formulaire
        [HttpPost]
        public ActionResult Create(ClientCreateForm form)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    ClientService service = new ClientService();
                    service.CreateClient(new ClientBO(form.Nom, form.Prenom, form.Email));
                    TempData["success"] = "Le client a été correctement ajouté";
                    return RedirectToAction("Index");
                }
                catch (UniqueEmailException ex)
                {
                    TempData["error"] = ex.Message;
                }
                catch (DbUpdateException)
                {
                    TempData["error"] = "La connection à la base de donnée a échoué";
                }
                catch (Exception)
                {
                    TempData["error"] = "Une erreur d'origine inconne est survenue";
                }
            }
            // si le formualire n'est pas valide je retorne vers la vue avec les erreurs
            return View(form);
        }
    }
}
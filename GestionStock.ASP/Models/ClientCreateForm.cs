﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestionStock.ASP.Models
{
    public class ClientCreateForm
    {
        [Required]
        [MaxLength(50)]
        [MinLength(2)]
        public string Nom { get; set; }

        [Required]
        [MaxLength(50)]
        [MinLength(2)]
        public string Prenom { get; set; }

        [Required]
        [MaxLength(255)]
        [EmailAddress]
        public string Email { get; set; }
    }
}
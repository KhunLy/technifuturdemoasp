﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionStock.ASP.Models
{
    public class ClientIndexModel
    {
        public string Reference { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
    }
}
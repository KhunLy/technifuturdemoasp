﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionStock.BLL.BusinessObjects
{
    public class ClientBO
    {
        public ClientBO(string nom, string prenom, string email)
        {
            Nom = nom;
            Prenom = prenom;
            Email = email;
        }

        public ClientBO(string reference, string nom, string prenom, string email)
            :this(nom, prenom, email)
        {
            Reference = reference;
        }

        public string Reference { get; }
        public string Nom { get; }
        public string Prenom { get; }
        public string Email { get; set; }
    }
}

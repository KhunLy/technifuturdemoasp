﻿using GestionStock.BLL.BusinessObjects;
using GestionStock.BLL.Exceptions;
using GestionStock.DAL;
using GestionStock.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionStock.BLL.Services
{
    public class ClientService
    {
        public IEnumerable<ClientBO> GetActiveClients()
        {
            using(StockContext context = new StockContext())
            {
                return context.Clients
                    .Where(c => c.Active)
                    // mapping
                    .ToList()
                    .Select(c => new ClientBO(c.Reference, c.Nom, c.Prenom, c.Email));
            }
        }

        public void CreateClient(ClientBO newClient) // de mon formulaire j'ai déjà nom, prenom, email
        {
            using (StockContext context = new StockContext())
            {
                // vérification email existant
                if (context.Clients.Any(c => c.Email == newClient.Email)) // any => linq: verifie si un élement correspond à une contidion
                    // déclencher une exception qui sera gérée plus tard par l'applicatif (ASP)
                    throw new UniqueEmailException();
                // création de la reférence
                string reference = CreateRef(newClient.Nom, newClient.Prenom);
                // création du client à ajouter dans la db
                Client toAdd = new Client
                {
                    Nom = newClient.Nom,
                    Prenom = newClient.Prenom,
                    Email = newClient.Email,
                    Reference = reference,
                    Active = true,
                };
                // ajouter le nouveau client dans le context d'entity framework
                context.Clients.Add(toAdd);
                // envoyer vers le serveur SQL
                context.SaveChanges();
            }
        }

        public void UpdateClient(string reference, string email)
        {
            throw new NotImplementedException();
        }

        public void DeleteClient(string reference)
        {
            throw new NotImplementedException();
        }

        private string CreateRef(string nom, string prenom)
        {
            string firstPart = nom.Substring(0,2) + prenom.Substring(0,2);
            using (StockContext context = new StockContext())
            {
                int count = context.Clients.Count();
                // si count + 1 < 10 ajouter 3 zeros
                // si count + 1 < 100 ajouter 2 zeros
                // ...
                string number = $"{count + 1}".PadLeft(4, '0'); // ajouter des 0 supplémentaires pour que la lng atteigne 4.
                return firstPart + number;
            }
        }
    }
}

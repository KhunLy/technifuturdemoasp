﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionStock.DAL.Entities
{
    [Table("Clients")]
    public class Client
    {
        [Key]
        [Column("reference", TypeName = "CHAR")]
        [MaxLength(8)]
        public string Reference {  get; set; } // 2 lettres du nom + 2 lettres du prenom + 0000

        [Column("nom", TypeName = "VARCHAR")]
        [MaxLength(50)]
        [Required]
        public string Nom { get; set; }

        [Column("prenom", TypeName = "VARCHAR")]
        [MaxLength(50)]
        [Required]
        public string Prenom { get; set; }

        [Column("email", TypeName = "VARCHAR")]
        [MaxLength(255)]
        [Required]
        [Index(IsUnique = true)]
        public string Email { get; set; }

        [Column("active", TypeName = "BIT")]
        [Required]
        public bool Active { get; set; }

        public ICollection<Commande> Commandes { get; set; }
    }
}

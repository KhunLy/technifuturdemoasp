﻿using GestionStock.DAL.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionStock.DAL.Entities
{
    [Table("Commandes")]
    public class Commande
    {
        [Key]
        [Column("id", TypeName = "INT")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)] // id auto incrementé
        public int Id { get; set; }
        
        [Column("date", TypeName = "DATETIME2")]
        [Required]
        public DateTime Date { get; set; }

        [Column("statut", TypeName = "INT")]
        [Required]
        public CommandeStatut Statut { get; set; } // InProgress, Pending, Closed, Canceled

        [Column("clientRef", TypeName = "CHAR")]
        [MaxLength(8)]
        [Required]
        public string ClientRef { get; set; }

        [ForeignKey("ClientRef")]
        public Client Client { get; set; }

        public ICollection<LigneCommande> LignesCommande { get; set; }
    }
}

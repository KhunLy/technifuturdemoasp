﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionStock.DAL.Entities
{
    [Table("LignesCommande")]
    public class LigneCommande
    {
        [Key]
        [Column("Id", TypeName = "INT" )]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("quantite", TypeName = "INT")]
        [Required]
        public int Quantite { get; set; }

        [Column("prix", TypeName = "MONEY")]
        [Required]
        public decimal Prix { get; set; }

        [Column("commandeId", TypeName = "INT")]
        [Required]
        public int CommandeId { get; set; }

        [Column("produitRef", TypeName = "CHAR")]
        [MaxLength(8)]
        [Required]
        public string ProduitRef { get; set; }

        [ForeignKey("CommandeId")]
        public Commande Commande { get; set; }

        [ForeignKey("ProduitRef")]
        public Produit Produit { get; set; }
    }
}

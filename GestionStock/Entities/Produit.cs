﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionStock.DAL.Entities
{
    [Table("Produits")]
    public class Produit
    {
        [Column("reference",TypeName = "CHAR")]
        [MaxLength(8)]
        [Key]
        public string Reference { get; set; }

        [Column("nom", TypeName = "VARCHAR")]
        [MaxLength(100)]
        [Required]
        public string Nom { get; set; }

        [Column("description", TypeName = "NVARCHAR(MAX)")]
        [Required]
        public string Description { get; set; }

        [Column("prix", TypeName = "MONEY")]
        [Required]
        public decimal Prix { get; set; }

        [Column("quantite", TypeName = "INT")]
        [Required]
        public int Quantite { get; set; }

        [Column("active", TypeName = "BIT")]
        [Required]
        public bool Active { get; set; }

        public ICollection<LigneCommande> LignesCommande { get; set; }
    }

}

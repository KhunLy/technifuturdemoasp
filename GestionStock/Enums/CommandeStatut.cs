﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionStock.DAL.Enums
{
    public enum CommandeStatut
    {
        EnCours, EnAttente, Fini, Annule
    }
}

﻿namespace GestionStock.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TableCustomer : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        reference = c.String(nullable: false, maxLength: 8, fixedLength: true, unicode: false),
                        nom = c.String(nullable: false, maxLength: 50, unicode: false),
                        prenom = c.String(nullable: false, maxLength: 50, unicode: false),
                        email = c.String(nullable: false, maxLength: 255, unicode: false),
                        active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.reference);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Clients");
        }
    }
}

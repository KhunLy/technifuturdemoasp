﻿namespace GestionStock.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TableCommande : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Commandes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        date = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        statut = c.Int(nullable: false),
                        clientRef = c.String(nullable: false, maxLength: 8, fixedLength: true, unicode: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Clients", t => t.clientRef, cascadeDelete: true)
                .Index(t => t.clientRef);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Commandes", "clientRef", "dbo.Clients");
            DropIndex("dbo.Commandes", new[] { "clientRef" });
            DropTable("dbo.Commandes");
        }
    }
}

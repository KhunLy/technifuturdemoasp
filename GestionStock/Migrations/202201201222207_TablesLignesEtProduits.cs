﻿namespace GestionStock.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TablesLignesEtProduits : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LignesCommande",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        quantite = c.Int(nullable: false),
                        prix = c.Decimal(nullable: false, storeType: "money"),
                        commandeId = c.Int(nullable: false),
                        produitRef = c.String(nullable: false, maxLength: 8, fixedLength: true, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Commandes", t => t.commandeId, cascadeDelete: true)
                .ForeignKey("dbo.Produits", t => t.produitRef, cascadeDelete: true)
                .Index(t => t.commandeId)
                .Index(t => t.produitRef);
            
            CreateTable(
                "dbo.Produits",
                c => new
                    {
                        reference = c.String(nullable: false, maxLength: 8, fixedLength: true, unicode: false),
                        nom = c.String(nullable: false, maxLength: 100, unicode: false),
                        description = c.String(nullable: false),
                        prix = c.Decimal(nullable: false, storeType: "money"),
                        quantite = c.Int(nullable: false),
                        active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.reference);
            
            CreateIndex("dbo.Clients", "email", unique: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LignesCommande", "produitRef", "dbo.Produits");
            DropForeignKey("dbo.LignesCommande", "commandeId", "dbo.Commandes");
            DropIndex("dbo.LignesCommande", new[] { "produitRef" });
            DropIndex("dbo.LignesCommande", new[] { "commandeId" });
            DropIndex("dbo.Clients", new[] { "email" });
            DropTable("dbo.Produits");
            DropTable("dbo.LignesCommande");
        }
    }
}

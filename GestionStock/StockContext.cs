﻿using GestionStock.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionStock.DAL
{
    public class StockContext: DbContext
    {
        public DbSet<Client> Clients { get; set; }

        public StockContext()
            //: base("server=K-PC;initial catalog=stock;uid=sa;pwd=Test1234=")
            : base("server=K-PC;initial catalog=stock;integrated security=true")
        {
            //Database.SetInitializer(new CreateDatabaseIfNotExists<StockContext>());
            // création automatique de le db si elle n'existe pas
        }
    }
}

﻿using GestionStock.DAL;
using GestionStock.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDAL
{
    internal class Program
    {
        static void Main(string[] args)
        {
            using (StockContext dc = new StockContext())
            {
                #region SELECT
                //List<Client> clients = dc.Clients
                //    .ToList(); // SELECT * FROM Client 
                #endregion



                #region Insert
                //Client newClient = new Client();
                //newClient.Nom = "Ly";
                //newClient.Prenom = "Khun";
                //newClient.Reference = "LYKH0001";
                //newClient.Email = "lykhun@gmail.com";
                //newClient.Active = true;
                //dc.Clients.Add(newClient); 
                #endregion

                #region Update
                //Client c = dc.Clients.Find("LYKH0001");
                //c.Email = "khun.ly@bstorm.be"; 
                #endregion

                #region Delete
                //Client c = dc.Clients.Find("LYKH0001");
                //dc.Clients.Remove(c); 
                #endregion



                var clients = dc.Clients.Select(c => new { c.Nom, c.Prenom }).ToList();

                var o = new { Id = 42, Nom = "Khun" };

                Student s = new Student() { Id = 42, Nom = "Khun" };

                dc.SaveChanges();
            }

        }
        class Student { public int Id { get; set; } public string Nom { get; set; } }
    }
}
